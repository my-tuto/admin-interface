Django==3.1.6
whitenoise==5.2.0
Pillow==8.1.0
django-admin-interface==0.15.2