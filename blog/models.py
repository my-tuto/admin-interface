from django.db import models

# Create your models here.

class Categorie(models.Model):
    """Model definition for Categorie."""

    # TODO: Define fields here
    name = models.CharField(max_length=255, null=True, blank=True)
    
    status = models.BooleanField(default=True)
    date_add = models.DateTimeField(auto_now_add=True)
    date_up = models.DateTimeField(auto_now=True)
    class Meta:
        """Meta definition for Categorie."""

        verbose_name = 'Categorie'
        verbose_name_plural = 'Categories'

    def __str__(self):
        """Unicode representation of Categorie."""
        return self.name

class Article(models.Model):
    """Model definition for Article."""

    # TODO: Define fields here
    image = models.ImageField(upload_to='blog/', default='blog.jpg')
    titre = models.CharField(max_length=255, null=True, blank=True)
    categorie = models.ManyToManyField(Categorie)
    description = models.TextField(null=True, blank=True)
    
    status = models.BooleanField(default=True)
    date_add = models.DateTimeField(auto_now_add=True)
    date_up = models.DateTimeField(auto_now=True)
    
    class Meta:
        """Meta definition for Article."""

        verbose_name = 'Article'
        verbose_name_plural = 'Articles'
        
    def get_absolute_url(self):
        return f'/blog/{self.slug}/single/'

    def __str__(self):
        """Unicode representation of Article."""
        return self.titre