from django.contrib import admin
from . import models
# Register your models here.

@admin.register(models.Categorie)
class CategorieAdmin(admin.ModelAdmin):
    list_display = ('name', 'status', 'date_add', 'date_up')
    list_filter = ('status', 'date_add', 'date_up')
    search_fields = ('name',)
    list_per_page = 3
    ordering = ['name',]

@admin.register(models.Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('titre', 'status', 'date_add', 'date_up')
    list_filter = ('status', 'date_add', 'date_up')
    search_fields = ('titre',)
    list_per_page = 3
    ordering = ['titre',]
    
    filter_horizontal = ('categorie',)